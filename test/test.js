const {checkAge, checkFullname} = require('../src/util.js');
const {assert, expect} = require('chai');



describe('test_checkAge',()=>{
    it('age_not_empty',()=> {
        const age = "";
        assert.isNotEmpty(checkAge(age));  
    });
    it('age_not_undefined',()=> {
        const age = undefined;
         
        assert.isDefined(checkAge(age));  
    });
    it('age_not_null',()=> {
        const age = null;
         
        assert.isNotNull(checkAge(age));  
    });
    it('age_not_zero',()=> {
        const age = 0;
        
        assert.isNotOk(age !==0,"age_is_not_zero");  
    });
    it('age_not_an_integer',()=> {
        const age = 2.5;
        
        // assert.isNotNull(checkAge(age));  
        assert.isNotOk( 
            Number.isInteger( checkAge(age) ) 
            ,"age_not_an_integer");  
    });
 
});

describe('test_checkFullname',()=>{
    it('name_not_empty',()=> {
        const name = "";
        assert.isNotEmpty(checkFullname(name));  
    });
    it('name_not_undefined',()=> {
        const name = undefined;
         
        assert.isDefined(checkFullname(name));  
    });
    it('name_not_null',()=> {
        const name = null;
         
        assert.isNotNull(checkFullname(name));  
    });
    it('name_not_string',()=> {
        const name = 2;
        
        assert.isNaN( checkFullname(name),`${typeof(checkFullname(name))} fullname_not_a_string`);  
    });
    
});
